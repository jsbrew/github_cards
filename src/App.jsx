import React from 'react';
import CardList from './components/cardList.jsx';
import Form from './components/form.jsx';

export class App extends React.Component {
/*	constructor(props) {
		super(props);
		this.state = {
			profiles: testData,
		};
	}*/

	// The above in short
	state = {
		profiles: [],
	};

	addnewProfile = (profileData) => {
		this.setState(prevState => ({
			profiles: [...prevState.profiles, profileData],
		}));
	}

	render() {
		return (
			<div>
				<div className='header'>
					<h1>{this.props.title}</h1>
				</div>
				<Form onSubmit={this.addnewProfile} />
				<CardList profiles={this.state.profiles} />
			</div>

		)
	}
}

export default App