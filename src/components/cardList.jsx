import React from 'react';
import Card from './card.jsx';

function CardList( { profiles } ) {

	const data = profiles;

	return (
		<div>
			{
				data.map(obj => 
					<Card key={obj.id} {...obj}/>
				)
			}
		</div>
	)
}

export default CardList